FROM rundeck/rundeck:4.5.0
LABEL maintainer="Caio Vinicius Maia Villela villela.caio@gmail.com"

ARG DEBIAN_FRONTEND=noninteractive

RUN echo "deb mirror://mirrors.ubuntu.com/mirrors.txt focal main restricted universe multiverse" |sudo tee /etc/apt/sources.list
RUN echo "deb mirror://mirrors.ubuntu.com/mirrors.txt focal main restricted universe multiverse" |sudo tee -a /etc/apt/sources.list
RUN echo "deb mirror://mirrors.ubuntu.com/mirrors.txt focal-updates main restricted universe multiverse" |sudo tee -a /etc/apt/sources.list
RUN echo "deb mirror://mirrors.ubuntu.com/mirrors.txt focal-backports main restricted universe multiverse" |sudo tee -a /etc/apt/sources.list
RUN echo "deb mirror://mirrors.ubuntu.com/mirrors.txt focal-security main restricted universe multiverse" |sudo tee -a /etc/apt/sources.list

RUN sudo apt-get clean && sudo apt-get update && sudo apt-get upgrade -y && sudo apt-get dist-upgrade -y 
RUN sudo DEBIAN_FRONTEND=noninteractive apt-get -qq install -y yamllint python3-pip hping3 ansible nmap jq vim wget sshpass curl nmon htop iotop telnet traceroute net-tools apt-transport-https software-properties-common gcc python-dev libkrb5-dev libssl-dev

# Adding powershell support
RUN sudo wget -q "https://packages.microsoft.com/config/ubuntu/$(lsb_release -rs)/packages-microsoft-prod.deb"
RUN sudo dpkg -i packages-microsoft-prod.deb
RUN sudo apt-get update && sudo DEBIAN_FRONTEND=noninteractive apt-get -qq install -y powershell

RUN pip3 install pywinrm
RUN sudo pip3 install pywinrm

RUN pip3 install pywinrm[kerberos]
RUN sudo pip3 install pywinrm[kerberos]

RUN pip3 install requests-kerberos
RUN sudo pip3 install requests-kerberos

RUN pip3 install pywinrm[credssp]
RUN sudo pip3 install pywinrm[credssp]

RUN pip3 install requests-credssp
RUN sudo pip3 install requests-credssp

RUN pip3 install pexpect
RUN sudo pip3 install pexpect

RUN sudo DEBIAN_FRONTEND=noninteractive apt-get install -y krb5-user
RUN sudo apt-get autoremove -y
RUN sudo apt-get autoclean -y

ADD docker-lib /home/rundeck/
RUN sudo chown rundeck:root /home/rundeck/docker-lib
RUN sudo chmod +x /home/rundeck/docker-lib/entry.sh

WORKDIR /home/rundeck/libext
RUN wget https://github.com/rundeck-plugins/ansible-plugin/releases/download/v3.2.2/ansible-plugin-3.2.2.jar
RUN wget https://github.com/rundeck-plugins/rundeck-winrm-plugin/releases/download/v1.3.8/rundeck-winrm-plugin-1.3.8.jar
RUN wget https://github.com/rundeck-plugins/py-winrm-plugin/releases/download/2.0.15/py-winrm-plugin-2.0.15.zip
RUN wget https://github.com/rundeck-plugins/openssh-node-execution/releases/download/2.0.2/openssh-node-execution-2.0.2.zip
RUN wget https://github.com/bageera/rundeck-team-webhook/releases/download/v0.7/rundeck-team-incoming-webhook-plugin-0.6.jar
RUN wget https://github.com/rundeck-plugins/slack-incoming-webhook-plugin/releases/download/v1.2.5/slack-incoming-webhook-plugin-1.2.5.jar
RUN wget https://github.com/rundeck-plugins/jq-json-logfilter/releases/download/1.0.4/jq-json-logfilter-1.0.4.jar
RUN wget https://github.com/rundeck-plugins/http-step/releases/download/1.0.12/http-step-1.0.12.jar
RUN wget https://github.com/rundeck-plugins/openssh-bastion-node-execution/releases/download/1.0.3/openssh-bastion-node-execution-1.0.3.zip
RUN wget https://github.com/rundeck-plugins/running-jobs-node-enhancer/releases/download/v1.0.0/running-jobs-node-enhancer-1.0.0.jar

WORKDIR /home/rundeck
ENTRYPOINT [ "/tini", "--", "docker-lib/entry.sh" ]
